from faker import Factory

fake = Factory.create('cs_CZ')

# # OR
# from faker import Faker
#
# fake = Faker()

print fake.name()
# 'Lucy Cechtelar'

# 10 x 'Lucy Cechtelar'
for name in range(0, 10):
    print fake.name()
    print fake.address()
    # "426 Jordy Lodge
    #  Cartwrightshire, SC 88120-6700"

fake.text()
# Sint velit eveniet. Rerum atque repellat voluptatem quia rerum. Numquam excepturi
# beatae sint laudantium consequatur. Magni occaecati itaque sint et sit tempore. Nesciunt
# amet quidem. Iusto deleniti cum autem ad quia aperiam.
# A consectetur quos aliquam. In iste aliquid et aut similique suscipit. Consequatur qui
# quaerat iste minus hic expedita. Consequuntur error magni et laboriosam. Aut aspernatur
# voluptatem sit aliquam. Dolores voluptatum est.
# Aut molestias et maxime. Fugit autem facilis quos vero. Eius quibusdam possimus est.
# Ea quaerat et quisquam. Deleniti sunt quam. Adipisci consequatur id in occaecati.
# Et sint et. Ut ducimus quod nemo ab voluptatum.


# Default language - 10 names
# for _ in range(0, 10):
#     print fake.name()
#
#     # Adaline Reichel
#     # Dr. Santa Prosacco DVM
#     # Noemy Vandervort V
#     # Lexi O'Conner
#     # Gracie Weber
#     # Roscoe Johns
#     # Emmett Lebsack
#     # Keegan Thiel
#     # Wellington Koelpin II
#     # Ms. Karley Kiehn V
